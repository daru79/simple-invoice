@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Dodaj produkt lub usługę do faktury</div>

                    <div class="card-body">

                        <div class="btn-group" style="float:right">
                            <a href="{{ route('product.create', $invoice->id()) }}" class="btn btn-primary">Nowy produkt</a>
                        </div>

                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nazwa</th>
                                <th scope="col">Jednostka miary</th>
                                <th scope="col">Stawka VAT</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($products as $product)
                                <tr>
                                    <th scope="row">{{ $product->id() }}</th>
                                    <td>{{ $product->name() }}</td>
                                    <td>{{ $product->measureUnit() }}</td>
                                    <td>{{ $product->taxRate() }}</td>
                                    <td>
                                        <div class="btn-group btn-group-sm" role="group" style="float:right">
                                            <a href="{{ route('invoice-product.create', [$invoice->id(), $product->id()]) }}" class="btn btn-info">Wybierz</a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

            <div class="col-md-12 mt-5">
                <div class="card">
                    <div class="card-header">Wybrane produkty lub usługi do faktury</div>

                    <div class="card-body">

                        <div class="btn-group" style="float:right">
                            <a href="{{ route('pdf.show', $invoice->id()) }}" class="btn btn-primary">Podgląd faktury</a>
                        </div>

                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nazwa</th>
                                <th scope="col">Jednostka miary</th>
                                <th scope="col">Ilość</th>
                                <th scope="col">Cena netto EUR</th>
                                <th scope="col">Kurs (z dnia)</th>
                                <th scope="col">Cena netto PLN</th>
                                <th scope="col">Wartość netto</th>
                                <th scope="col">Stawka VAT</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($invoiceProducts as $invoiceProduct)
                                <tr>
                                    <th scope="row">{{ $invoiceProduct->id() }}</th>
                                    <td>{{ $invoiceProduct->name() }}</td>
                                    <td>{{ $invoiceProduct->measureUnit() }}</td>
                                    <td>{{ $invoiceProduct->quantity() }}</td>
                                    <td>{{ $invoiceProduct->priceEur() === null ? '-' : Str::currency($invoiceProduct->priceEur()) }}</td>
                                    <td>{{ $invoiceProduct->exchangeRate() === null ? '-' : $invoiceProduct->exchangeRate().' ('.$invoiceProduct->exchangeDate().')' }}</td>
                                    <td>{{ $invoiceProduct->pricePln() === null ? '-' : Str::currency($invoiceProduct->pricePln()) }}</td>
                                    <td>{{ $invoiceProduct->priceEur() !== null ? Str::currency($invoiceProduct->priceEur() * $invoiceProduct->quantity()) : Str::currency($invoiceProduct->pricePln() * $invoiceProduct->quantity()) }}</td>
                                    <td>{{ $invoiceProduct->taxRate() }}%</td>
                                    <td>
                                        <div class="btn-group btn-group-sm" role="group" style="float:right">
                                            <a href="{{ route('invoice-product.edit', [$invoice->id(), $invoiceProduct->id()]) }}" class="btn btn-success">Edytuj</a>
                                            <a href="{{ route('invoice-product.destroy', $invoiceProduct->id()) }}" class="btn btn-danger">Usuń</a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                                <tr>
                                    <td colspan="9" style="text-align: right"><b>Podsumowanie faktury:</b></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="9" style="text-align: right">Razem netto w EUR:</td>
                                    <td>{{ Str::currency($sumEur) }}</td>
                                </tr>
                                <tr>
                                    <td colspan="9" style="text-align: right">Razem netto w PLN:</td>
                                    <td>{{ Str::currency($sumPln) }}</td>
                                </tr>
                                <tr>
                                    <td colspan="9" style="text-align: right">Razem VAT w PLN:</td>
                                    <td>{{ Str::currency($sumVat) }}</td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
