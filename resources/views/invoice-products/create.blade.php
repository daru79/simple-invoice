@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Produkt {{ $product->name() }}</div>

                <div class="card-body">

                    <form method="POST" action="{{ route('invoice-product.store', $invoice->id()) }}">
                        @csrf
                        @method('POST')
                        <div class="form-group">
                            <label for="nip">Nazwa *</label>
                            <input name="name" type="text" class="form-control" id="name" value="{{ $product->name() }}">

                            @error('name')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label for="measure_unit">Jednostka miary</label>
                            <input name="measure_unit" type="text" class="form-control" id="measure_unit" value="{{ $product->measureUnit() }}">

                            @error('measure_unit')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label for="tax_rate">Stawka VAT {{ $product->taxRate() }}</label>

                            <select class="form-select" name="tax_rate" id="tax_rate">
                                <option value="0"
                                @if($product->taxRate() === "0")
                                    selected
                                @endif
                                >zwolniony</option>
                                <option value="5"
                                @if($product->taxRate() === "5")
                                        selected
                                @endif
                                >5</option>
                                <option value="8"
                                @if($product->taxRate() === "8")
                                    selected
                                @endif
                                >8</option>
                                <option value="23"
                                @if($product->taxRate() === "23")
                                    selected
                                @endif
                                >23</option>
                            </select>

                            @error('tax_rate')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label for="quantity">Ilość</label>
                            <input name="quantity" type="text" class="form-control" id="quantity" value="{{ old('quantity', 1) }}">

                            @error('quantity')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label for="price_eur">Cena jedn. netto [EUR]</label>
                            <input name="price_eur" type="text" class="form-control" id="price_eur" value="{{ old('price_eur') }}">

                            @error('price_eur')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label for="exchange_date">Data kursu EUR/PLN</label>
                            <input name="exchange_date" type="date" class="form-control" id="exchange_date" value="{{ old('exchange_date') }}">

                            @error('exchange_date')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label for="price_pln">Cena jedn. netto [PLN]</label>
                            <input name="price_pln" type="text" class="form-control" id="price_pln" value="">

                            @error('price_pln')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <hr>
                        <div class="btn-group" style="float:right">
                            <button type="submit" class="btn btn-primary">Dodaj</button>
                            <a href="" class="btn btn-secondary">Anuluj</a>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
