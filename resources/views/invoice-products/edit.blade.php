@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Produkt {{ $invoiceProduct->name() }}</div>

                <div class="card-body">

                    <form method="POST" action="{{ route('invoice-product.update', [$invoiceId, $invoiceProduct->id()]) }}">
                        @csrf
                        @method('POST')
                        <div class="form-group">
                            <label for="nip">Nazwa *</label>
                            <input name="name" type="text" class="form-control" id="name" value="{{ $invoiceProduct->name() }}">

                            @error('name')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label for="measure_unit">Jednostka miary</label>
                            <input name="measure_unit" type="text" class="form-control" id="measure_unit" value="{{ $invoiceProduct->measureUnit() }}">

                            @error('measure_unit')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label for="tax_rate">Stawka VAT {{ $invoiceProduct->taxRate() }}</label>

                            <select class="form-select" name="tax_rate" id="tax_rate">
                                <option value="0"
                                @if($invoiceProduct->taxRate() === "0")
                                    selected
                                @endif
                                >zwolniony</option>
                                <option value="5"
                                @if($invoiceProduct->taxRate() === "5")
                                        selected
                                @endif
                                >5</option>
                                <option value="8"
                                @if($invoiceProduct->taxRate() === "8")
                                    selected
                                @endif
                                >8</option>
                                <option value="23"
                                @if($invoiceProduct->taxRate() === "23")
                                    selected
                                @endif
                                >23</option>
                            </select>

                            @error('tax_rate')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label for="quantity">Ilość</label>
                            <input name="quantity" type="text" class="form-control" id="quantity" value="{{ old('quantity', $invoiceProduct->quantity()) }}">

                            @error('quantity')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label for="price_eur">Cena jedn. netto [EUR]</label>
                            <input name="price_eur" type="text" class="form-control" id="price_eur" value="{{ old('price_eur', $invoiceProduct->priceEur()) }}">

                            @error('price_eur')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label for="exchange_date">Data kursu EUR/PLN</label>
                            <input name="exchange_date" type="dete" class="form-control" id="exchange_date" value="{{ old('exchange_date', $invoiceProduct->exchangeDate()) }}">

                            @error('exchange_date')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label for="price_pln">Cena jedn. netto [PLN]</label>
                            <input name="price_pln" type="text" class="form-control" id="price_pln" value="{{ old('price_pln', $invoiceProduct->pricePln()) }}">

                            @error('price_pln')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <hr>
                        <div class="btn-group" style="float:right">
                            <button type="submit" class="btn btn-primary">Dodaj</button>
                            <a href="" class="btn btn-secondary">Anuluj</a>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
