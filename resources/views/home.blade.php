@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
        </div>

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Przegląd kontrahentów</div>

                <div class="card-body">

                    <div class="btn-group" style="float:right">
                        <a href="{{ route('contractor.create') }}" class="btn btn-secondary">Nowy kontrahent</a>
                    </div>

                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">NIP</th>
                            <th scope="col">REGON</th>
                            <th scope="col">Nazwa</th>
                            <th scope="col">Adres</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($contractors as $contractor)
                            <tr>
                                <th scope="row">{{ $contractor->id() }}</th>
                                <td>{{ $contractor->nip() }}</td>
                                <td>{{ $contractor->regon() }}</td>
                                <td>{{ $contractor->name() }}</td>
                                <td>
                                    {{ $contractor->street() }},
                                    {{ $contractor->zip() }}
                                    {{ $contractor->city() }}
                                </td>
                                <td>
                                    <div class="btn-group btn-group-sm" role="group" style="float: right;">
                                        <a href="{{ route('contractor.show', $contractor->id()) }}" class="btn btn-info">Szczegóły</a>
                                        <a href="{{ route('contractor.edit', $contractor->id()) }}" class="btn btn-success">Edytuj</a>
                                        <a href="{{ route('contractor.destroy', $contractor->id()) }}" class="btn btn-danger">Usuń</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
