@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Edycja kontrahenta</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('contractor.update', $contractor->id()) }}">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="nip">NIP *</label>
                            <input name="nip" type="text" class="form-control" id="nip" value="{{ $contractor->nip() }}">

                            @error('nip')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label for="regon">REGON</label>
                            <input name="regon" type="text" class="form-control" id="regon" value="{{ $contractor->regon() }}">

                            @error('regon')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label for="name">Nazwa *</label>
                            <input name="name" type="text" class="form-control" id="name" value="{{ $contractor->name() }}">

                            @error('name')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label for="street">Ulica *</label>
                            <input name="street" type="text" class="form-control" id="street" value="{{ $contractor->street() }}">

                            @error('street')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label for="zip">Kod pocztowy *</label>
                            <input name="zip" type="text" class="form-control" id="zip" value="{{ $contractor->zip() }}">

                            @error('zip')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label for="city">Miasto *</label>
                            <input name="city" type="text" class="form-control" id="city" value="{{ $contractor->city() }}">

                            @error('city')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <hr>
                        <div class="btn-group" style="float:right">
                            <button type="submit" class="btn btn-primary">Dodaj</button>
                            <a href="{{ route('home') }}" class="btn btn-secondary">Anuluj</a>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
