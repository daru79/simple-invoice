@extends('layouts.pdf')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <table class="table">
                            <thead>
                            <tr>
                                <td rowspan="2"><h3><b>LUK-TRANS<br>Łukasz Łozicki</b></h3></td>
                                <td colspan="2"><h5><b>Faktura VAT {{ $invoice->signature() }}</b></h5></td>
                            </tr>

                            <tr>
                                <td>
                                    Data sprzedaży:<br>
                                    Termin płatności:<br>
                                    Sposób zapłaty:
                                </td>
                                <td>
                                    {{ $invoice->issueDate() }}<br>
                                    {{ $invoice->dueDate() }}<br>
                                    {{ $invoice->paymentMethod() }}
                                </td>
                            </tr>
                            </thead>
                        </table>
                    </div>

                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">Sprzedawca:</th>
                                <th scope="col">Nabywca:</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    {{ $ownerData['company']['name'] }}<br>
                                    {{ $ownerData['company']['address'] }}<br>
                                    {{ $ownerData['company']['zip'] }}
                                    {{ $ownerData['company']['city'] }}<br>
                                    Konto:<br>
                                    {{ $ownerData['bank']['account'] }}<br>
                                    {{ $ownerData['bank']['account'] }}
                                </td>
                                <td>
                                    {{ $contractorData->name() }}<br>
                                    NIP: {{ $contractorData->nip() }}<br>
                                    REGON: {{ $contractorData->regon() }}<br>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">Pozycje faktury:</th>
                            </tr>
                            </thead>
                        </table>

                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th scope="col">Lp.</th>
                                <th scope="col">Nazwa towaru lub usługi</th>
                                <th scope="col">Ilość</th>
                                <th scope="col">Jedn.</th>
                                <th scope="col">Cena jedn. netto</th>
                                <th scope="col">Wartość netto</th>
                                <th scope="col">Stawka VAT</th>
                                <th scope="col">Wartość VAT w PLN</th>
                                <th scope="col">Wartość brutto</th>
                            </tr>
                            </thead>
                            <tbody>

                            @php
                                $i = 0
                            @endphp
                            @foreach($invoiceProducts as $invoiceProduct)
                            <tr>
                                <th scope="row">{{ ++$i }}</th>
                                <td>
                                    {{ $invoiceProduct->name() }}
                                @if($invoiceProduct->priceEur() !== null)
                                (kurs {{ $invoiceProduct->exchangeRate() }} z dnia {{ $invoiceProduct->exchangeDate() }})
                                @endif
                                </td>
                                <td style="text-align: right">{{ $invoiceProduct->quantity() }}</td>
                                <td style="text-align: right">{{ $invoiceProduct->measureUnit() }}</td>
                                @if($invoiceProduct->priceEur() !== null)
                                <td style="text-align: right">{{ Str::currency($invoiceProduct->priceEur()) }} &euro;</td>
                                @else
                                <td style="text-align: right">{{ Str::currency($invoiceProduct->pricePln()) }} zł</td>
                                @endif
                                @if($invoiceProduct->priceEur() !== null)
                                    <td style="text-align: right">{{ Str::currency($invoiceProduct->priceEur() * $invoiceProduct->quantity()) }} &euro;</td>
                                @else
                                    <td style="text-align: right">{{ Str::currency($invoiceProduct->pricePln() * $invoiceProduct->quantity()) }} zł</td>
                                @endif
                                <td>{{ $invoiceProduct->taxRate() }} %</td>
                                @if($invoiceProduct->priceEur() !== null)
                                    <td style="text-align: right">{{ Str::currency($invoiceProduct->priceEur() * $invoiceProduct->quantity() * ($invoiceProduct->taxRate()/100) * $invoiceProduct->exchangeRate()) }} zł</td>
                                @else
                                    <td style="text-align: right">{{ Str::currency($invoiceProduct->pricePln() * $invoiceProduct->quantity() * $invoiceProduct->taxRate()/100) }} zł</td>
                                @endif
                                @if($invoiceProduct->priceEur() !== null)
                                    <td style="text-align: right">{{ Str::currency($invoiceProduct->priceEur() * $invoiceProduct->quantity() * ($invoiceProduct->taxRate()/100 + 1)) }} &euro;</td>
                                @else
                                    <td style="text-align: right">{{ Str::currency($invoiceProduct->pricePln() * $invoiceProduct->quantity() * ($invoiceProduct->taxRate()/100 + 1)) }} zł</td>
                                @endif
                            </tr>
                            @endforeach

                            </tbody>
                        </table>

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">Podsumowanie:</th>
                            </tr>
                            </thead>
                        </table>

                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col">Wartość netto</th>
                                <th scope="col">Kwota VAT</th>
                                <th scope="col">Wartość brutto</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th>Razem:</th>
                                @if($sumEur !== 0.0)
                                    <td style="text-align: right">{{ Str::currency($sumEur) }} &euro;</td>
                                @else
                                    <td style="text-align: right">{{ Str::currency($sumPln) }} zł</td>
                                @endif

                                @if($sumVatEur !== 0.0)
                                    <td style="text-align: right">{{ Str::currency($sumVatEur) }} &euro;</td>
                                @else
                                    <td style="text-align: right">{{ Str::currency($sumVatPln) }} zł</td>
                                @endif

                                @if($sumEur !== 0.0)
                                    <td style="text-align: right">{{ Str::currency($sumEur + $sumVatEur) }} &euro;</td>
                                @else
                                    <td style="text-align: right">{{ Str::currency($sumPln + $sumVatPln) }} zł</td>
                                @endif
                            </tr>
                            <tr>
                                <th>Do zapłaty:</th>
                                @if($sumEur !== 0.0)
                                    <td colspan="3" style="text-align: right"><b>{{ Str::currency($sumEur + $sumVatEur) }} &euro;</b></td>
                                @else
                                    <td colspan="3" style="text-align: right"><b>{{ Str::currency($sumPln + $sumVatPln) }} zł</b></td>
                                @endif
                            </tr>
                            </tbody>
                        </table>

                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Uwagi:</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($sumEur !== 0.0)
                            <tr>
                                <td>Kwota VAT w przeliczeniu na złotówki wynosi {{ Str::currency($sumVat) }} zł.</td>
                            </tr>
                            @endif
                            @if($invoice->comments() !== null)
                            <tr>
                                <td>{{ $invoice->comments() }}</td>
                            </tr>
                            @endif
                            </tbody>
                        </table>

                        <table class="table">
                            <tbody>
                            <tr>
                                <td style="text-align: left;">Faktura bez podpisu odbiorcy</td>
                                <td style="text-align: right;">Osoba upoważniona do wystawienia faktury VAT<br><b>Łukasz Łozicki</b></td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
