@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ $contractor->name() }}</div>

                    <div class="card-body">

                        <div class="btn-group" style="float:right">
                            <a href="{{ route('invoice.create', $contractor->id()) }}" class="btn btn-primary">Nowa faktura</a>
                        </div>

                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Sygnatura</th>
                                <th scope="col">Data wystawienia</th>
                                <th scope="col">Termin płatności</th>
                                <th scope="col">Metoda płatności</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($invoices as $invoice)
                                <tr>
                                    <th scope="row">{{ $invoice->id() }}</th>
                                    <td>{{ $invoice->signature() }}</td>
                                    <td>{{ $invoice->issueDate() }}</td>
                                    <td>{{ $invoice->dueDate() }}</td>
                                    <td>{{ $invoice->paymentMethod() }}</td>
                                    <td>
                                        <div class="btn-group btn-group-sm" role="group" style="float: right;">
                                            <a href="{{ route('pdf.show', $invoice->id()) }}" class="btn btn-info">Podgląd</a>
                                            <a href="{{ route('invoice-product.index', $invoice->id()) }}" class="btn btn-success">Edytuj</a>
                                            <a href="{{ route('invoice.destroy', $invoice->id()) }}" class="btn btn-danger">Usuń</a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
