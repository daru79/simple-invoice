@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Nowa faktura dla {{ $contractor->name() }}</div>

                <div class="card-body">

                    <form method="POST" action="{{ route('invoice.store', $contractor->id()) }}">
                        @csrf
                        @method('POST')
                        <div class="form-group">
                            <label for="signature">Sygnatura</label>
                            <input name="signature" type="text" class="form-control" id="signature" value="{{ old('signature') }}">

                            @error('signature')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label for="payment_method">Forma płatności</label>
                            <select class="form-select" name="payment_method" id="payment_method">
                                <option value="cash">gotówka</option>
                                <option value="transfer" selected>przelew</option>
                            </select>

                            @error('payment_method')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label for="comments">Uwagi</label>
                            <textarea name="comments"class="form-control" id="comments">{{ old('comments') }}</textarea>

                            @error('comments')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label for="issue_date">Data wystawienia</label>
                            <input name="issue_date" type="date" placeholder="RRRR-MM-DD" class="form-control" id="issue_date" value="{{ old('issue_date') }}">

                            @error('issue_date')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label for="due_date">Termin płatności</label>
                            <select class="form-select" name="due_date" id="due_date">
                                <option value="7">7 dni</option>
                                <option value="14" selected>14 dni</option>
                            </select>

                            @error('due_date')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <hr>
                        <div class="btn-group" style="float:right">
                            <button type="submit" class="btn btn-primary">Dodaj</button>
                            <a href="{{ route('home') }}" class="btn btn-secondary">Anuluj</a>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
