@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Nowy produkt</div>

                <div class="card-body">

                    <form method="POST" action="{{ route('product.store', $invoiceId) }}">
                        @csrf
                        @method('POST')
                        <div class="form-group">
                            <label for="nip">Nazwa *</label>
                            <input name="name" type="text" class="form-control" id="name" value="{{ old('name') }}">

                            @error('name')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label for="measure_unit">Jednostka miary</label>
                            <input name="measure_unit" type="text" class="form-control" id="measure_unit" value="{{ old('measure_unit') }}">

                            @error('measure_unit')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label for="tax_rate">Stawka VAT</label>

                            <select class="form-select" name="tax_rate" id="tax_rate">
                                <option value="0">zwolniony</option>
                                <option value="5">5</option>
                                <option value="8">8</option>
                                <option value="23" selected>23</option>
                            </select>

                            @error('tax_rate')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                        <hr>
                        <div class="btn-group" style="float:right">

                            @if ($invoiceId === null)
                                <button type="submit" class="btn btn-primary">Dodaj</button>
                            @else
                                <button type="submit" class="btn btn-primary">Dodaj</button>
                            @endif

                            @if ($invoiceId === null)
                                <a href="{{ route('product.index') }}" class="btn btn-secondary">Anuluj</a>
                            @else
                                <a href="{{ route('invoice-product.index', $invoiceId) }}" class="btn btn-secondary">Anuluj</a>
                            @endif
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
