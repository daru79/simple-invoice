@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Wykaz produktów i usług</div>

                    <div class="card-body">

                        <div class="btn-group" style="float:right">
                            <a href="{{ route('product.create') }}" class="btn btn-primary">Nowy produkt</a>
                        </div>

                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nazwa</th>
                                <th scope="col">Jednostka miary</th>
                                <th scope="col">Stawka VAT</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($products as $product)
                                <tr>
                                    <th scope="row">{{ $product->id() }}</th>
                                    <td>{{ $product->name() }}</td>
                                    <td>{{ $product->measureUnit() }}</td>
                                    <td>{{ $product->taxRate() }}</td>
                                    <td>
                                        <div class="btn-group btn-group-sm" role="group" style="float:right">
                                            <a href="" class="btn btn-success">Edytuj</a>
                                            <a href="{{ route('product.destroy', $product->id()) }}" class="btn btn-danger">Usuń</a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
