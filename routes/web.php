<?php

use App\Http\Controllers\ContractorController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\InvoiceItemController;
use App\Http\Controllers\InvoicePdfController;
use App\Http\Controllers\InvoiceProductController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('logout', function ()
{
    auth()->logout();
    Session()->flush();

    return Redirect::to('/');
})->name('logout');

Route::get('/register', function () {
    return view('auth.register');
})->name('register');

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::get('/contractor', [ContractorController::class, 'create'])->name('contractor.create');
Route::post('/contractor/create', [ContractorController::class, 'store'])->name('contractor.store');
Route::get('/contractor/show/{contractor}', [ContractorController::class, 'show'])->name('contractor.show');
Route::get('/contractor/delete/{contractor}', [ContractorController::class, 'destroy'])->name('contractor.destroy');
Route::get('/contractor/edit/{contractor}', [ContractorController::class, 'edit'])->name('contractor.edit');
Route::put('/contractor/update/{contractor}', [ContractorController::class, 'update'])->name('contractor.update');

Route::get('/product', [ProductController::class, 'index'])->name('product.index');
Route::get('/product/create/{invoice}', [ProductController::class, 'create'])->name('product.create');
Route::post('/product/store/{invoice}', [ProductController::class, 'store'])->name('product.store');
Route::get('/product/delete/{product}', [ProductController::class, 'destroy'])->name('product.destroy');

Route::get('/invoice/create/{contractor}', [InvoiceController::class, 'create'])->name('invoice.create');
Route::post('/invoice/store/{contractor}', [InvoiceController::class, 'store'])->name('invoice.store');
Route::get('/invoice/delete/{invoice}', [InvoiceController::class, 'destroy'])->name('invoice.destroy');

Route::get('/invoice-product/{invoice}', [InvoiceProductController::class, 'index'])->name('invoice-product.index');
Route::get('/invoice-product/create/{invoice}/{product}', [InvoiceProductController::class, 'create'])->name('invoice-product.create');
Route::get('/invoice-product/edit/{invoice}/{invoiceProduct}', [InvoiceProductController::class, 'edit'])->name('invoice-product.edit');
Route::post('/invoice-product/update/{invoice}/{invoiceProduct}', [InvoiceProductController::class, 'update'])->name('invoice-product.update');
Route::get('/invoice-product/delete/{invoiceProduct}', [InvoiceProductController::class, 'destroy'])->name('invoice-product.destroy');
// TODO usunac ten ponizszy kontroler i przerzucic do InvoiceProductController
Route::post('/invoice-product/store/{invoice}', [InvoiceItemController::class, 'store'])->name('invoice-product.store');

Route::get('/pdf/show/{invoice}', [InvoicePdfController::class, 'show'])->name('pdf.show');
