<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    public function up(): void
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('contractor_id');
            $table->string('signature');
            $table->string('payment_method');
            $table->string('comments')->nullable(true)->default(true);
            $table->string('issue_date');
            $table->string('due_date');
            $table->timestamps();

            $table
                ->foreign('contractor_id')
                ->references('id')
                ->on('contractors')
                ->onDelete('cascade');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('invoices');
    }
}
