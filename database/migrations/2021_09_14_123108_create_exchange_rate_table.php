<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExchangeRateTable extends Migration
{
    public function up(): void
    {
        Schema::create('exchange_rates', function (Blueprint $table) {
            $table->id();
            $table->string('date');
            $table->float('rate');
            $table->timestamps();
        });
    }
    public function down(): void
    {
        Schema::dropIfExists('exchange_rates');
    }
}
