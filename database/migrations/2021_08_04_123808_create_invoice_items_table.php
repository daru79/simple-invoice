<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceItemsTable extends Migration
{
    public function up(): void
    {
        Schema::create('invoice_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('invoice_id');
            $table->string('name');
            $table->string('quantity');
            $table->string('measure_unit')->nullable(true);
            $table->string('tax_rate')->nullable(true);
            $table->string('price_eur')->nullable(true);
            $table->string('exchange_rate')->nullable(true);
            $table->string('exchange_date')->nullable(true);
            $table->string('price_pln')->nullable(true);
            $table->timestamps();

            $table
                ->foreign('invoice_id')
                ->references('id')
                ->on('invoices')
                ->onDelete('cascade');
        });
    }
    public function down(): void
    {
        Schema::dropIfExists('invoice_items');
    }
}
