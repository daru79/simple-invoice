<?php

declare(strict_types=1);

namespace App\Services\Contractor;

final class Contractor
{
    private ?int $id;
    private string $nip;
    private ?string $regon;
    private string $name;

    public function __construct(
        ?int $id,
        string $nip,
        ?string $regon,
        string $name,
        string $street,
        string $zip,
        string $city
    ) {
        $this->id = $id;
        $this->nip = $nip;
        $this->regon = $regon;
        $this->name = $name;
        $this->street = $street;
        $this->zip = $zip;
        $this->city = $city;
    }

    public function id(): ?int
    {
        return $this->id;
    }

    public function nip(): string
    {
        return $this->nip;
    }

    public function regon(): ?string
    {
        return $this->regon;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function city(): string
    {
        return $this->city;
    }

    public function street(): string
    {
        return $this->street;
    }

    public function zip(): string
    {
        return $this->zip;
    }
}
