<?php

declare(strict_types=1);

namespace App\Services\Contractor;

use App\Models\Contractor as ContractorModel;
use Illuminate\Support\Collection;

class ContractorRepository
{
    public function findAll(): Collection
    {
        return ContractorModel::all()
            ->map(fn (ContractorModel $contractor): Contractor => new Contractor
            (
                $contractor->id,
                $contractor->nip,
                $contractor->regon,
                $contractor->name,
                $contractor->street,
                $contractor->zip,
                $contractor->city,
            ));
    }

    public function save(Contractor $contractor): void
    {
        $contractorModel = new ContractorModel();
        $contractorModel->id = $contractor->id();
        $contractorModel->nip = $contractor->nip();
        $contractorModel->regon = $contractor->regon();
        $contractorModel->name = $contractor->name();
        $contractorModel->street = $contractor->street();
        $contractorModel->zip = $contractor->zip();
        $contractorModel->city = $contractor->city();

        $contractorModel->saveOrFail();
    }

    public function update(Contractor $contractor): void
    {
        ContractorModel::whereId($contractor->id())
            ->update([
                'nip' => $contractor->nip(),
                'regon' => $contractor->regon(),
                'name' => $contractor->name(),
                'street' => $contractor->street(),
                'zip' => $contractor->zip(),
                'city' => $contractor->city(),
            ]);
    }

    public function findById(int $id): ?Contractor
    {
        $contractorModel = ContractorModel::find($id);

        if ($contractorModel == null) {
            return null;
        }

        return $this->mapModelToDTO($contractorModel);
    }

    public function delete(int $id): void
    {
        $contractorModel = ContractorModel::find($id);

        if ($contractorModel === null) {
            throw new \InvalidArgumentException('Nie znaleziono kontrahenta.');
        }

        $contractorModel->delete($id);
    }

    private function mapModelToDTO(ContractorModel $model): Contractor
    {
        return new Contractor(
            $model->id,
            $model->nip,
            $model->regon,
            $model->name,
            $model->street,
            $model->zip,
            $model->city,
        );
    }
}
