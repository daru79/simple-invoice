<?php

declare(strict_types=1);

namespace App\Services\NBP;

use Illuminate\Http\Client\Response as ResponseClient;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;

class ExchangeRateProvider
{
    private const URL = 'http://api.nbp.pl/api/exchangerates/rates/A/EUR/%s?format=json';

    private ExchangeRateRepository $repository;

    public function __construct()
    {
        $this->repository = new ExchangeRateRepository();
    }

    public function exchangeRate(string $date): ?float
    {
        $exchangeRate = $this->repository->findByDate($date);

        if ($exchangeRate !== null) {
            return $exchangeRate->rate();
        } else {
            $response = Http::get(sprintf(self::URL, $date));

            if ($response->status() === Response::HTTP_NOT_FOUND) {
                return null;
            }

            $exchangeRateData = json_decode($response->body(),true);
            $rate = $exchangeRateData['rates'][0]['mid'];

            $exchangeRate = new ExchangeRate(null, $date, $rate);

            $rate = $this->repository->save($exchangeRate);

            return $rate;
        }
    }
}
