<?php

declare(strict_types=1);

namespace App\Services\NBP;

use App\Models\ExchangeRate as ExchangeRateModel;

class ExchangeRateRepository
{
    public function findByDate(string $date): ?ExchangeRate
    {
        $exchangeRateModel = ExchangeRateModel::firstWhere('date', $date);

        if ($exchangeRateModel === null) {
            return null;
        }

        return $this->mapModelToDTO($exchangeRateModel);
    }

    public function save(ExchangeRate $exchangeRate): float
    {
        $exchangeRateModel = new ExchangeRateModel();
        $exchangeRateModel->id = $exchangeRate->id();
        $exchangeRateModel->date = $exchangeRate->date();
        $exchangeRateModel->rate = $exchangeRate->rate();

        $exchangeRateModel->saveOrFail();

        return $exchangeRateModel->rate;
    }

    private function mapModelToDTO(ExchangeRateModel $model): ExchangeRate
    {
        return new ExchangeRate(
            $model->id,
            $model->date,
            (float) $model->rate
        );
    }
}
