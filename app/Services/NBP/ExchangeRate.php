<?php

declare(strict_types=1);

namespace App\Services\NBP;

class ExchangeRate
{
    private ?int $id;
    private string $date;
    private float $rate;

    public function __construct(?int $id, string $date, float $rate)
    {
        $this->id = $id;
        $this->date = $date;
        $this->rate = $rate;
    }

    public function id(): ?int
    {
        return $this->id;
    }

    public function date(): string
    {
        return $this->date;
    }

    public function rate(): float
    {
        return $this->rate;
    }
}
