<?php

declare(strict_types=1);

namespace App\Services\InvoiceItem;

use App\Models\InvoiceItem as InvoiceItemModel;
use App\Services\InvoiceItem\InvoiceItem;

class InvoiceItemRepository
{
//    public function findAll(int $contractorId): Collection
//    {
//        /** @var InvoiceModel|Collection $invoices */
//        $invoices = InvoiceModel::where('contractor_id', $contractorId)
//            ->orderBy('id')
//            ->get();
//
//        return $invoices->map(fn (InvoiceModel $invoice): Invoice => new Invoice($invoice->id, null, $invoice->signature, $invoice->payment_method, $invoice->comments, $invoice->issue_date, $invoice->due_date));
//    }

    public function save(InvoiceItem $invoiceItem): int
    {
        $invoiceItemModel = new InvoiceItemModel();
        $invoiceItemModel->id = $invoiceItem->id();
        $invoiceItemModel->invoice_id = $invoiceItem->invoice()->id();
        $invoiceItemModel->name = $invoiceItem->name();
        $invoiceItemModel->quantity = $invoiceItem->quantity();
        $invoiceItemModel->measure_unit = $invoiceItem->measureUnit();
        $invoiceItemModel->tax_rate = $invoiceItem->taxRate();
        $invoiceItemModel->price_eur = $invoiceItem->priceEur();
        $invoiceItemModel->exchange_rate = $invoiceItem->exchangeRate();
        $invoiceItemModel->exchange_date = $invoiceItem->exchangeDate();
        $invoiceItemModel->price_pln = $invoiceItem->pricePln();

        $invoiceItemModel->saveOrFail();

        return $invoiceItemModel->id;
    }

//    public function update(Contractor $contractor): void
//    {
//        ContractorModel::whereId($contractor->id())
//            ->update([
//                'nip' => $contractor->nip(),
//                'regon' => $contractor->regon(),
//                'name' => $contractor->name(),
//            ]);
//    }

//    public function findById(int $id): Invoice
//    {
//        $invoiceModel = InvoiceModel::find($id);
//
//        return $this->mapModelToDTO($invoiceModel);
//    }

//    public function delete(int $id): void
//    {
//        $contractorModel = ContractorModel::find($id);
//
//        if ($contractorModel === null) {
//            throw new \InvalidArgumentException('Nie znaleziono kontrahenta.');
//        }
//
//        $contractorModel->delete($id);
//    }
//
//    private function mapModelToDTO(InvoiceModel $model): Invoice
//    {
//        return new Invoice(
//            $model->id,
//            null,
//            $model->signature,
//            $model->payment_method,
//            $model->comments,
//            $model->issue_date,
//            $model->due_date,
//        );
//    }
}
