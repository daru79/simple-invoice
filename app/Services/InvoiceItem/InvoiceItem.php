<?php

declare(strict_types=1);

namespace App\Services\InvoiceItem;

use App\Services\Contractor\Contractor;
use App\Services\Invoice\Invoice;

class InvoiceItem
{
    private ?int $id;
    private ?Invoice $invoice;
    private string $name;
    private string $quantity;
    private ?string $measureUnit;
    private ?string $taxRate;
    private ?string $priceEur;
    private ?float $exchangeRate;
    private ?string $exchangeDate;
    private ?string $pricePln;

    public function __construct
    (
        ?int $id,
        ?Invoice $invoice,
        string $name,
        string $quantity,
        ?string $measureUnit,
        ?string $taxRate,
        ?string $priceEur,
        ?float $exchangeRate,
        ?string $exchangeDate,
        ?string $pricePln
    ) {
        $this->id = $id;
        $this->invoice = $invoice;
        $this->name = $name;
        $this->quantity = $quantity;
        $this->measureUnit = $measureUnit;
        $this->taxRate = $taxRate;
        $this->priceEur = $priceEur;
        $this->exchangeRate = $exchangeRate;
        $this->exchangeDate = $exchangeDate;
        $this->pricePln = $pricePln;
    }

    public function id(): ?int
    {
        return $this->id;
    }

    public function invoice(): ?Invoice
    {
        return $this->invoice;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function quantity(): string
    {
        return $this->quantity;
    }

    public function measureUnit(): ?string
    {
        return $this->measureUnit;
    }

    public function taxRate(): ?string
    {
        return $this->taxRate;
    }

    public function priceEur(): ?string
    {
        return $this->priceEur;
    }

    public function exchangeRate(): ?float
    {
        return $this->exchangeRate;
    }

    public function exchangeDate(): ?string
    {
        return $this->exchangeDate;
    }

    public function pricePln(): ?string
    {
        return $this->pricePln;
    }
}
