<?php

declare(strict_types=1);

namespace App\Services\InvoiceProduct;

use App\Models\Invoice as InvoiceModel;
use App\Models\InvoiceItem as InvoiceItemsModel;
use App\Models\InvoiceProduct as InvoiceProductModel;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class InvoiceProductRepository
{
    public function findAll(int $invoiceId): Collection
    {
        /** @var InvoiceItemsModel|Collection $invoices */
        $invoices = InvoiceItemsModel::where('invoice_id', $invoiceId)
            ->orderBy('id')
            ->get();

        return $invoices->map(fn (InvoiceItemsModel $invoiceProduct): InvoiceProduct => new InvoiceProduct($invoiceProduct->id, null, $invoiceProduct->name, $invoiceProduct->measure_unit, $invoiceProduct->quantity, $invoiceProduct->tax_rate, $invoiceProduct->price_eur, $invoiceProduct->exchange_rate, $invoiceProduct->exchange_date, $invoiceProduct->price_pln));
    }

    public function save(InvoiceProduct $invoiceItem): int
    {
        $invoiceItemModel = new InvoiceItemsModel();
        $invoiceItemModel->id = $invoiceItem->id();
        $invoiceItemModel->invoice_id = $invoiceItem->invoice()->id();
        $invoiceItemModel->name = $invoiceItem->name();
        $invoiceItemModel->measure_unit = $invoiceItem->measureUnit();
        $invoiceItemModel->tax_rate = $invoiceItem->taxRate();
        $invoiceItemModel->price_eur = $invoiceItem->priceEur();
        $invoiceItemModel->exchange_rate = $invoiceItem->exchangeRate();
        $invoiceItemModel->exchange_date = $invoiceItem->exchangeDate();
        $invoiceItemModel->price_pln = $invoiceItem->pricePln();

        $invoiceItemModel->saveOrFail();

        return $invoiceItemModel->id;
    }

    public function update(InvoiceProduct $invoiceProduct): void
    {
        InvoiceItemsModel::whereId($invoiceProduct->id())
            ->update([
                'invoice_id' => $invoiceProduct->invoice()->id(),
                'name' => $invoiceProduct->name(),
                'quantity' => $invoiceProduct->quantity(),
                'measure_unit' => $invoiceProduct->measureUnit(),
                'tax_rate' => $invoiceProduct->taxRate(),
                'price_eur' => $invoiceProduct->priceEur(),
                'exchange_rate' => $invoiceProduct->exchangeRate(),
                'exchange_date' => $invoiceProduct->exchangeDate(),
                'price_pln' => $invoiceProduct->pricePln(),
            ]);
    }

    public function findById(int $id): ?InvoiceProduct
    {
        $invoiceProductModel = InvoiceItemsModel::find($id);

        if ($invoiceProductModel === null) {
            return null;
        }

        return $this->mapModelToDTO($invoiceProductModel);
    }

    public function delete(int $id): void
    {
        $invoiceProductModel = InvoiceItemsModel::find($id);

        if ($invoiceProductModel === null) {
            throw new \InvalidArgumentException('Nie znaleziono produktu/usługi.');
        }

        $invoiceProductModel->delete($id);
    }

    private function mapModelToDTO(InvoiceItemsModel $model): InvoiceProduct
    {
        return new InvoiceProduct(
            $model->id,
            null,
            $model->name,
            $model->measure_unit,
            $model->quantity,
            $model->tax_rate,
            $model->price_eur,
            null,
            $model->exchange_date,
            $model->price_pln
        );
    }
}
