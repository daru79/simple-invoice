<?php

declare(strict_types=1);

namespace App\Services\InvoiceProduct;

use Illuminate\Support\Collection;

class InvoiceProductsCalculator
{
    private float $sumPln;
    private float $sumEur;
    private float $sumVat;
    private float $sumVatEur;
    private float $sumVatPln;
    private Collection $invoiceProducts;

    public function __construct(Collection $invoiceProducts)
    {
        $this->sumPln = 0;
        $this->sumEur = 0;
        $this->sumVat = 0;
        $this->sumVatEur = 0;
        $this->sumVatPln = 0;
        $this->invoiceProducts = $invoiceProducts;
        $this->sum();
    }

    public function sumPln(): float
    {
        return $this->sumPln;
    }

    public function sumEur(): float
    {
        return $this->sumEur;
    }

    public function sumVat(): float
    {
        return $this->sumVat;
    }

    public function sumVatEur(): float
    {
        return $this->sumVatEur;
    }

    public function sumVatPln(): float
    {
        return $this->sumVatPln;
    }

    private function sum(): void
    {
        $this->invoiceProducts->each(function (InvoiceProduct $invoiceProduct) {
            $this->sumPln += $invoiceProduct->quantity() * $invoiceProduct->pricePln();
            $this->sumEur += $invoiceProduct->quantity() * $invoiceProduct->priceEur();

            if ($this->sumPln !== 0.0) {
                $this->sumVat += ($invoiceProduct->taxRate()/100) * $invoiceProduct->quantity() * $invoiceProduct->pricePln();
                $this->sumVatPln += ($invoiceProduct->taxRate()/100) * $invoiceProduct->quantity() * $invoiceProduct->pricePln();
            } else {
                $this->sumVat += ($invoiceProduct->taxRate()/100) * $invoiceProduct->quantity() * $invoiceProduct->priceEur() * $invoiceProduct->exchangeRate();
                $this->sumVatEur += ($invoiceProduct->taxRate()/100) * $invoiceProduct->quantity() * $invoiceProduct->priceEur();
            }
        });
    }
}
