<?php

declare(strict_types=1);

namespace App\Services\InvoiceProduct;

use App\Services\Contractor\Contractor;
use App\Services\Invoice\Invoice;

class InvoiceProduct
{
    private ?int $id;
    private ?Invoice $invoice;
    private string $name;
    private ?string $measureUnit;
    private ?string $quantity;
    private ?string $taxRate;
    private ?string $priceEur;
    private ?string $exchangeRate;
    private ?string $exchangeDate;
    private ?string $pricePln;

    public function __construct
    (
        ?int $id,
        ?Invoice $invoice,
        string $name,
        ?string $measureUnit,
        ?string $quantity,
        ?string $taxRate,
        ?string $priceEur,
        ?string $exchangeRate,
        ?string $exchangeDate,
        ?string $pricePln
    ) {
        $this->id = $id;
        $this->invoice = $invoice;
        $this->name = $name;
        $this->measureUnit = $measureUnit;
        $this->quantity = $quantity;
        $this->taxRate = $taxRate;
        $this->priceEur = $priceEur;
        $this->exchangeRate = $exchangeRate;
        $this->exchangeDate = $exchangeDate;
        $this->pricePln = $pricePln;
    }

    public function id(): ?int
    {
        return $this->id;
    }

    public function invoice(): ?Invoice
    {
        return $this->invoice;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function measureUnit(): ?string
    {
        return $this->measureUnit;
    }

    public function quantity(): ?string
    {
        return $this->quantity;
    }

    public function taxRate(): ?string
    {
        return $this->taxRate;
    }

    public function priceEur(): ?string
    {
        return $this->priceEur;
    }

    public function exchangeRate(): ?string
    {
        return $this->exchangeRate;
    }

    public function exchangeDate(): ?string
    {
        return $this->exchangeDate;
    }

    public function pricePln(): ?string
    {
        return $this->pricePln;
    }
}
