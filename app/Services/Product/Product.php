<?php

declare(strict_types=1);

namespace App\Services\Product;

class Product
{
    private ?int $id;
    private string $name;
    private ?string $measureUnit;
    private ?string $taxRate;

    public function __construct(
        ?int $id,
        string $name,
        ?string $measureUnit,
        ?string $taxRate
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->measureUnit = $measureUnit;
        $this->taxRate = $taxRate;
    }

    public function id(): ?int
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function measureUnit(): ?string
    {
        return $this->measureUnit;
    }

    public function taxRate(): ?string
    {
        return $this->taxRate;
    }
}
