<?php

declare(strict_types=1);

namespace App\Services\Product;

use App\Models\Product as ProductModel;
use Illuminate\Support\Collection;

class ProductRepository
{
    public function findAll(): Collection
    {
        /** @var ProductModel|Collection $products */
        $products = ProductModel::all();

        return $products->map(fn (ProductModel $product): Product => new Product($product->id, $product->name, $product->measure_unit, $product->tax_rate));
    }

    public function save(Product $product): void
    {
        $productModel = new ProductModel();
        $productModel->id = $product->id();
        $productModel->name = $product->name();
        $productModel->measure_unit = $product->measureUnit();
        $productModel->tax_rate = $product->taxRate();
        $productModel->saveOrFail();
    }
//
//    public function update(Contractor $contractor): void
//    {
//        ContractorModel::whereId($contractor->id())
//            ->update([
//                'nip' => $contractor->nip(),
//                'regon' => $contractor->regon(),
//                'name' => $contractor->name(),
//            ]);
//    }
//
    public function findById(int $id): Product
    {
        $productModel = ProductModel::find($id);

        return $this->mapModelToDTO($productModel);
    }

    public function delete(int $id): void
    {
        $productModel = ProductModel::find($id);

        if ($productModel === null) {
            throw new \InvalidArgumentException('Nie znaleziono produktu/usługi.');
        }

        $productModel->delete($id);
    }

    private function mapModelToDTO(ProductModel $model): Product
    {
        return new Product(
            $model->id,
            $model->name,
            $model->measure_unit,
            $model->tax_rate
        );
    }
}
