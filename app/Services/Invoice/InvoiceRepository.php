<?php

declare(strict_types=1);

namespace App\Services\Invoice;

use App\Models\Invoice as InvoiceModel;
use Illuminate\Support\Collection;

class InvoiceRepository
{
    public function findAll(int $contractorId): Collection
    {
        /** @var InvoiceModel|Collection $invoices */
        $invoices = InvoiceModel::where('contractor_id', $contractorId)
            ->orderBy('id')
            ->get();

        return $invoices->map(fn (InvoiceModel $invoice): Invoice => new Invoice($invoice->id, null, $invoice->signature, $invoice->payment_method, $invoice->comments, $invoice->issue_date, $invoice->due_date));
    }

    public function save(Invoice $invoice): int
    {
        $invoiceModel = new InvoiceModel();
        $invoiceModel->id = $invoice->id();
        $invoiceModel->contractor_id = $invoice->contractor_id();
        $invoiceModel->signature = $invoice->signature();
        $invoiceModel->payment_method = $invoice->paymentMethod();
        $invoiceModel->comments = $invoice->comments();
        $invoiceModel->issue_date = $invoice->issueDate();
        $invoiceModel->due_date = $invoice->dueDate();
        $invoiceModel->saveOrFail();

        return $invoiceModel->id;
    }

//    public function update(Contractor $contractor): void
//    {
//        ContractorModel::whereId($contractor->id())
//            ->update([
//                'nip' => $contractor->nip(),
//                'regon' => $contractor->regon(),
//                'name' => $contractor->name(),
//            ]);
//    }

    public function findById(int $id): ?Invoice
    {
        $invoiceModel = InvoiceModel::find($id);

        if ($invoiceModel === null) {
            return null;
        }

        return $this->mapModelToDTO($invoiceModel);
    }

    public function delete(int $id): void
    {
        $invoiceModel = InvoiceModel::find($id);

        if ($invoiceModel === null) {
            throw new \InvalidArgumentException('Nie znaleziono faktury.');
        }

        $invoiceModel->delete($id);
    }

    private function mapModelToDTO(InvoiceModel $model): Invoice
    {
        return new Invoice(
            $model->id,
            (int) $model->contractor_id,
            $model->signature,
            $model->payment_method,
            $model->comments,
            $model->issue_date,
            $model->due_date,
        );
    }
}
