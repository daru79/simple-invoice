<?php

declare(strict_types=1);

namespace App\Services\Invoice;

use App\Services\Contractor\Contractor;
use Illuminate\Support\Carbon;

class Invoice
{
    private ?int $id;
    private ?int $contractor_id;
    private string $signature;
    private string $paymentMethod;
    private ?string $comments;
    private string $issueDate;
    private string $dueDate;

    public function __construct
    (
        ?int $id,
        ?int $contractor_id,
        string $signature,
        string $paymentMethod,
        ?string $comments,
        string $issueDate,
        string $dueDate
    ) {
        $this->id = $id;
        $this->contractor_id = $contractor_id;
        $this->signature = $signature;
        $this->paymentMethod = $paymentMethod;
        $this->comments = $comments;
        $this->issueDate = $issueDate;
        $this->dueDate = $dueDate;
    }

    public function id(): ?int
    {
        return $this->id;
    }

    public function contractor_id(): ?int
    {
        return $this->contractor_id;
    }

    public function signature(): string
    {
        return $this->signature;
    }

    public function paymentMethod(): string
    {
        return $this->paymentMethod;
    }

    public function comments(): ?string
    {
        return $this->comments;
    }

    public function issueDate(): string
    {
        return $this->issueDate;
    }

    public function dueDate(): string
    {
        return $this->dueDate;
    }
}
