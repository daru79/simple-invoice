<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreInvoiceProductRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:100',
            'tax_rate' => 'required',
            'quantity' => 'required',
            'price_eur' => 'required_with:exchange_date',
            'exchange_date' => 'required_with:price_eur',
            'price_pln' => 'required_without:price_eur|required_without:exchange_date',
        ];
    }
}
