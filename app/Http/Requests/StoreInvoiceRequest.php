<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreInvoiceRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'signature' => 'required|string|max:50',
            'issue_date' => 'required|date_format:Y-m-d',
            'due_date' => 'required',
        ];
    }
}
