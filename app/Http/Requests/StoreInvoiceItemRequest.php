<?php

declare(strict_types=1);

namespace App\Http\Requests;

use App\Services\NBP\ExchangeRateProvider;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

class StoreInvoiceItemRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:100',
            'tax_rate' => 'required',
            'quantity' => 'required',
            'price_eur' => 'required_with:exchange_date',
            'exchange_date' => 'required_with:price_eur|exchange_rate_exists',
            'price_pln' => 'required_without:price_eur|required_without:exchange_date',
        ];
    }

    public function messages()
    {
        return [
            'exchange_rate_exists' => 'Kurs z podanego dnia nie istnieje'
        ];
    }

    protected function getValidatorInstance(): Validator
    {
        /** @var Validator $validator */
        $validator = parent::getValidatorInstance();
        $validator->addImplicitExtension(
            'exchange_rate_exists',
            function ($attribute, $value): bool {
                $exchangeRateProvider = new ExchangeRateProvider();

                if ($value === null) {
                    return true;
                }

                $exchangeRate = $exchangeRateProvider->exchangeRate($value);

                if ($exchangeRate !== null) {
                    return true;
                }

                return false;
            }
        );

        return $validator;
    }
}
