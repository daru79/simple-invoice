<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateContractorRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'nip' => 'required|unique:App\Models\Contractor,nip,'.$this->route('contractor'),
            'regon' => 'nullable|unique:App\Models\Contractor,regon,'.$this->route('contractor'),
            'name' => 'required|string|max:100',
            'street' => 'required|string|max:100',
            'zip' => 'required|string|max:20',
            'city' => 'required|string|max:50',
        ];
    }
}
