<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\StoreContractorRequest;
use App\Http\Requests\StoreInvoiceProductRequest;
use App\Http\Requests\UpdateContractorRequest;
use App\Http\Requests\UpdateInvoiceProductRequest;
use App\Services\Contractor\Contractor;
use App\Services\Contractor\ContractorRepository;
use App\Services\Invoice\Invoice;
use App\Services\Invoice\InvoiceRepository;
use App\Services\InvoiceItem\InvoiceItem;
use App\Services\InvoiceProduct\InvoiceProduct;
use App\Services\InvoiceProduct\InvoiceProductRepository;
use App\Services\InvoiceProduct\InvoiceProductsCalculator;
use App\Services\NBP\ExchangeRateRepository;
use App\Services\Product\ProductRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\View\View;

class InvoiceProductController extends Controller
{
    private ContractorRepository $contractorRepository;
    private InvoiceRepository $invoiceRepository;
    private InvoiceProductRepository $invoiceProductRepository;
    private ProductRepository $productRepository;
    private ExchangeRateRepository $exchangeRateRepository;

    public function __construct
    (
        ContractorRepository $contractorRepository,
        InvoiceRepository $invoiceRepository,
        InvoiceProductRepository $invoiceProductRepository,
        ProductRepository $productRepository,
        ExchangeRateRepository $exchangeRateRepository
    ) {
        $this->middleware('auth');
        $this->contractorRepository = $contractorRepository;
        $this->invoiceRepository = $invoiceRepository;
        $this->invoiceProductRepository = $invoiceProductRepository;
        $this->productRepository = $productRepository;
        $this->exchangeRateRepository = $exchangeRateRepository;
    }

    public function index(int $invoiceId): View
    {
        $invoice = $this->invoiceRepository->findById($invoiceId);

        if ($invoice == null) {
            abort(Response::HTTP_NOT_FOUND);
        }

        $products = $this->productRepository->findAll();

        $invoiceProducts = $this->invoiceProductRepository->findAll($invoiceId);

        $calculator = new InvoiceProductsCalculator($invoiceProducts);

        return view('invoice-products/index', [
            'products' => $products,
            'invoice' => $invoice,
            'invoiceProducts' => $invoiceProducts,
            'sumPln' => $calculator->sumPln(),
            'sumEur' => $calculator->sumEur(),
            'sumVat' => $calculator->sumVat(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(int $invoiceId, int $productId): View
    {
        $invoice = $this->invoiceRepository->findById($invoiceId);

        $product = $this->productRepository->findById($productId);

        if ($product == null || $invoice == null) {
            abort(Response::HTTP_NOT_FOUND);
        }

        return view('invoice-products/create',
            [
                'invoice' => $invoice,
                'product' => $product,
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreInvoiceProductRequest $request, int $invoiceId): RedirectResponse
    {
        // TODO do uzupelnienia z InvoiceItemController::store
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id): View
    {
        $invoices = $this->invoiceRepository->findAll($id);

        $contractor = $this->contractorRepository->findById($id);

        return view('invoices/index', [
            'invoices' => $invoices,
            'contractor' => $contractor
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(int $invoiceId, int $invoiceProductId): View
    {
        $invoiceProduct = $this->invoiceProductRepository->findById($invoiceProductId);

        if ($invoiceProduct === null) {
            abort(Response::HTTP_NOT_FOUND);
        }

        return view('invoice-products/edit',
            [
                'invoiceProduct' => $invoiceProduct,
                'invoiceId' => $invoiceId,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateInvoiceProductRequest $request, int $invoiceId, int $invoiceProductId): RedirectResponse
    {
        if ($this->invoiceProductRepository->findById($invoiceProductId) === null) {
            abort(Response::HTTP_NOT_FOUND);
        }

        $invoice = $this->invoiceRepository->findById($invoiceId);

        if ($invoice === null) {
            abort(Response::HTTP_NOT_FOUND);
        }

        $invoiceProductToUpdate = $this->makeInvoiceProductFromRequest($request, $invoice, $invoiceProductId);

        $this->invoiceProductRepository->update($invoiceProductToUpdate);

        return redirect()->route('invoice-product.index', $invoiceId);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id): RedirectResponse
    {
        try {
            $this->invoiceProductRepository->delete($id);
        } catch (\InvalidArgumentException $exception) {
            return redirect('home')->with('error', $exception->getMessage());
        }

        return back();
    }

    private function makeInvoiceProductFromRequest(FormRequest $request, Invoice $invoice, int $invoiceProductId): InvoiceProduct
    {
        $rate = null;

        if ($request->input('exchange_date') !== null) {
            $exchangeRate = $this->exchangeRateRepository->findByDate($request->input('exchange_date'));
            $rate = (string) $exchangeRate->rate();
        }

        return new InvoiceProduct(
            $invoiceProductId,
            $invoice,
            $request->input('name'),
            $request->input('measure_unit'),
            $request->input('quantity'),
            $request->input('tax_rate'),
            $request->input('price_eur'),
            $rate,
            $request->input('exchange_date'),
            $request->input('price_pln'),
        );
    }
}
