<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\StoreInvoiceItemRequest;
use App\Services\Invoice\InvoiceRepository;
use App\Services\InvoiceItem\InvoiceItemRepository;
use App\Services\InvoiceItem\InvoiceItem;
use App\Services\NBP\ExchangeRateProvider;
use App\Services\NBP\ExchangeRateRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;

class InvoiceItemController extends Controller
{
    private InvoiceRepository $invoiceRepository;
    private InvoiceItemRepository $invoiceItemRepository;
    private ExchangeRateRepository $exchangeRateRepository;
    private ExchangeRateProvider $exchangeRateProvider;

    public function __construct
    (
        InvoiceRepository $invoiceRepository,
        InvoiceItemRepository $invoiceItemRepository,
        ExchangeRateRepository $exchangeRateRepository,
        ExchangeRateProvider $exchangeRateProvider
    ) {
        $this->invoiceRepository = $invoiceRepository;
        $this->invoiceItemRepository = $invoiceItemRepository;
        $this->exchangeRateRepository = $exchangeRateRepository;
        $this->exchangeRateProvider = $exchangeRateProvider;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreInvoiceItemRequest $request, int $invoiceId): RedirectResponse
    {
        $invoice = $this->invoiceRepository->findById($invoiceId);

        if ($invoice === null) {
            abort(Response::HTTP_NOT_FOUND);
        }

        $rate = null;

        if ($request->input('exchange_date') !== null) {
            $exchangeRate = $this->exchangeRateRepository->findByDate($request->input('exchange_date'));
            $rate = $exchangeRate->rate();
        }

        $invoiceItem = new InvoiceItem
        (
            null,
            $invoice,
            $request->input('name'),
            $request->input('quantity'),
            $request->input('measure_unit'),
            $request->input('tax_rate'),
            $request->input('price_eur'),
            $rate,
            $request->input('exchange_date'),
            $request->input('price_pln')
        );

        $this->invoiceItemRepository->save($invoiceItem);

        return redirect()->route('invoice-product.index', $invoiceId);
    }
}
