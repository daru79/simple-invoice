<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\Contractor\ContractorRepository;
use App\Services\Invoice\InvoiceRepository;
use App\Services\InvoiceProduct\InvoiceProductRepository;
use App\Services\InvoiceProduct\InvoiceProductsCalculator;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\App;

class InvoicePdfController extends Controller
{
    private const INVOICE_SIGNATURE = 'invoice_%s.pdf';
    private InvoiceRepository $invoiceRepository;
    private InvoiceProductRepository $invoiceProductRepository;
    private ContractorRepository $contractorRepository;
    private InvoiceProductsCalculator $invoiceProductsCalculator;

    public function __construct
    (
        InvoiceRepository $invoiceRepository,
        InvoiceProductRepository $invoiceProductRepository,
        ContractorRepository $contractorRepository,
        InvoiceProductsCalculator $invoiceProductsCalculator
    )
    {
        $this->invoiceRepository = $invoiceRepository;
        $this->invoiceProductRepository = $invoiceProductRepository;
        $this->contractorRepository = $contractorRepository;
        $this->invoiceProductsCalculator = $invoiceProductsCalculator;
    }

    public function show(int $invoiceId)
    {
        $invoice = $this->invoiceRepository->findById($invoiceId);

        $invoiceProducts = $this->invoiceProductRepository->findAll($invoiceId);

        $ownerData = [
            'company' => [
                'name' => config('owner-data.company.name'),
                'address' => config('owner-data.company.address'),
                'zip' => config('owner-data.company.zip'),
                'city' => config('owner-data.company.city'),
            ],
            'bank' => [
                'account' => config('owner-data.bank.account'),
                'iban' => config('owner-data.bank.iban'),
            ],
        ];

        $contractorData = $this->contractorRepository->findById($invoice->contractor_id());

        $calculator = new InvoiceProductsCalculator($invoiceProducts);

        $invoicePdf = PDF::loadView('pdf/show',
            [
                'invoice' => $invoice,
                'invoiceProducts' => $invoiceProducts,
                'ownerData' => $ownerData,
                'contractorData' => $contractorData,
                'sumPln' => $calculator->sumPln(),
                'sumEur' => $calculator->sumEur(),
                'sumVat' => $calculator->sumVat(),
                'sumVatEur' => $calculator->sumVatEur(),
                'sumVatPln' => $calculator->sumVatPln(),
            ]
        )->setPaper('a4', 'portrait');

        return $invoicePdf->download(sprintf(self::INVOICE_SIGNATURE, $invoiceId));
    }
}
