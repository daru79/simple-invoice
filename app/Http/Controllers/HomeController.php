<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\Contractor\ContractorRepository;

class HomeController extends Controller
{
    private ContractorRepository $contractorRepository;

    public function __construct(ContractorRepository $contractorRepository)
    {
        $this->middleware('auth');
        $this->contractorRepository = $contractorRepository;
    }

    /**
     * Show the application dashboard.
     */
    public function index()
    {
        $contractors = $this->contractorRepository->findAll();

        return view('home', [
            'contractors' => $contractors,
        ]);
    }
}
