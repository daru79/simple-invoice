<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\StoreContractorRequest;
use App\Http\Requests\StoreInvoiceRequest;
use App\Http\Requests\UpdateContractorRequest;
use App\Services\Contractor\Contractor;
use App\Services\Contractor\ContractorRepository;
use App\Services\Invoice\Invoice;
use App\Services\Invoice\InvoiceRepository;
use App\Services\Product\ProductRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\Request;

class InvoiceController extends Controller
{
    private ProductRepository $productRepository;
    private InvoiceRepository $invoiceRepository;
    private ContractorRepository $contractorRepository;

    public function __construct
    (
        ProductRepository $productRepository,
        InvoiceRepository $invoiceRepository,
        ContractorRepository $contractorRepository
    ) {
        $this->middleware('auth');
        $this->productRepository = $productRepository;
        $this->invoiceRepository = $invoiceRepository;
        $this->contractorRepository = $contractorRepository;
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(int $id): View
    {
        $contractor = $this->contractorRepository->findById($id);
        $products = $this->productRepository->findAll();

        return view('invoices/create', [
            'contractor' => $contractor,
            'products' => $products,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreInvoiceRequest $request, int $contractorId): RedirectResponse
    {
        $contractor = $this->contractorRepository->findById($contractorId);

        if ($contractor == null) {
            abort(404);
        }

        $invoice = new Invoice(
            null,
            $contractor->id(),
            $request->input('signature'),
            $request->input('payment_method'),
            $request->input('comments'),
            $request->input('issue_date'),
            $request->input('due_date')
        );

        $invoiceId = $this->invoiceRepository->save($invoice);

        return redirect()->route('invoice-product.index', $invoiceId);
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id): View
    {
        $invoices = $this->invoiceRepository->findAll($id);

        $contractor = $this->contractorRepository->findById($id);

        return view('invoices/index', [
            'invoices' => $invoices,
            'contractor' => $contractor
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(int $id): View
    {
        $contractor = $this->contractorRepository->findById($id);

        if ($contractor === null) {
            abort(404);
        }

        return view('contractors/edit',
            [
                'contractor' => $contractor,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateContractorRequest $request, int $id): RedirectResponse
    {
        try {
            $this->contractorRepository->findById($id);
        } catch (ModelNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        }

        $contractorToUpdate = $this->makeContractorFromRequest($request, $id);

        $this->contractorRepository->update($contractorToUpdate);

        // dodac flasha
        return redirect()->route('home');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id): RedirectResponse
    {
        try {
            $this->invoiceRepository->delete($id);
        } catch (\InvalidArgumentException $exception) {
            return redirect('home')->with('error', $exception->getMessage());
        }

        return back();
    }

    private function makeContractorFromRequest(FormRequest $request, int $id): Contractor
    {
        return new Contractor(
            $id,
            $request->input('nip'),
            $request->input('regon'),
            $request->input('name'),
        );
    }
}
