<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateContractorRequest;
use App\Services\Contractor\Contractor;
use App\Services\Product\Product;
use App\Services\Product\ProductRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\View\View;

class ProductController extends Controller
{
    private ProductRepository $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->middleware('auth');
        $this->repository = $repository;
    }

    public function index(): View
    {
        $products = $this->repository->findAll();

        return view('products/index', [
            'products' => $products,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(?int $invoiceId = null): View
    {
        return view('products/create',
            [
                'invoiceId' => $invoiceId
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreProductRequest $request, int $invoiceId = null): RedirectResponse
    {
        $product = new Product
        (
            null,
            $request->input('name'),
            $request->input('measure_unit'),
            $request->input('tax_rate'),
        );

        $this->repository->save($product);

        if ($invoiceId === null) {
            return redirect()->route('product.index');
        }

        return redirect()->route('invoice-product.index', $invoiceId);
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id): View
    {
        $invoices = $this->invoiceRepository->findAll($id);

        $contractor = $this->contractorRepository->findById($id);

        return view('invoices/index', [
            'invoices' => $invoices,
            'contractor' => $contractor
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(int $id): View
    {
        $contractor = $this->contractorRepository->findById($id);

        if ($contractor === null) {
            abort(404);
        }

        return view('contractors/edit',
            [
                'contractor' => $contractor,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateContractorRequest $request, int $id): RedirectResponse
    {
        try {
            $this->contractorRepository->findById($id);
        } catch (ModelNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        }

        $contractorToUpdate = $this->makeContractorFromRequest($request, $id);

        $this->contractorRepository->update($contractorToUpdate);

        // dodac flasha
        return redirect()->route('home');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id): RedirectResponse
    {
        try {
            $this->repository->delete($id);
        } catch (\InvalidArgumentException $exception) {
            return redirect('product.index')->with('error', $exception->getMessage());
        }

        return back();
    }

    private function makeContractorFromRequest(FormRequest $request, int $id): Contractor
    {
        return new Contractor(
            $id,
            $request->input('nip'),
            $request->input('regon'),
            $request->input('name'),
        );
    }
}
