<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\StoreContractorRequest;
use App\Http\Requests\UpdateContractorRequest;
use App\Services\Contractor\Contractor;
use App\Services\Contractor\ContractorRepository;
use App\Services\Invoice\Invoice;
use App\Services\Invoice\InvoiceRepository;
use App\Services\InvoiceProduct\InvoiceProduct;
use App\Services\InvoiceProduct\InvoiceProductRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\View\View;

class ContractorController extends Controller
{
    private ContractorRepository $contractorRepository;
    private InvoiceRepository $invoiceRepository;

    public function __construct
    (
        ContractorRepository $contractorRepository,
        InvoiceRepository $invoiceRepository
    ) {
        $this->middleware('auth');
        $this->contractorRepository = $contractorRepository;
        $this->invoiceRepository = $invoiceRepository;
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        return view('contractors/create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreContractorRequest $request): RedirectResponse
    {
        $contractor = new Contractor
        (
            null,
            $request->input('nip'),
            $request->input('regon'),
            $request->input('name'),
            $request->input('street'),
            $request->input('zip'),
            $request->input('city'),
        );

        $this->contractorRepository->save($contractor);

        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id): View
    {
        $invoices = $this->invoiceRepository->findAll($id);

        $contractor = $this->contractorRepository->findById($id);

        return view('invoices/index', [
            'invoices' => $invoices,
            'contractor' => $contractor
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(int $id): View
    {
        $contractor = $this->contractorRepository->findById($id);

        if ($contractor === null) {
            abort(404);
        }

        return view('contractors/edit',
            [
                'contractor' => $contractor,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateContractorRequest $request, int $id): RedirectResponse
    {
        try {
            $this->contractorRepository->findById($id);
        } catch (ModelNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        }

        $contractorToUpdate = $this->makeContractorFromRequest($request, $id);

        $this->contractorRepository->update($contractorToUpdate);

        // dodac flasha
        return redirect()->route('home');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id): RedirectResponse
    {
        try {
            $this->contractorRepository->delete($id);
        } catch (\InvalidArgumentException $exception) {
            return redirect('home')->with('error', $exception->getMessage());
        }

        return back();
    }

    private function makeContractorFromRequest(FormRequest $request, int $id): Contractor
    {
        return new Contractor(
            $id,
            $request->input('nip'),
            $request->input('regon'),
            $request->input('name'),
            $request->input('street'),
            $request->input('zip'),
            $request->input('city'),
        );
    }
}
