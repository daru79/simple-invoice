<?php

return [
    'company' => [
        'name' => 'LUK-TRANS Łukasz Łozicki',
        'address' => 'Sadowa 14/4',
        'zip' => '41-922',
        'city' => 'Radzionków'
    ],
    'bank' => [
        'account' => '1050-1050-1050',
        'iban' => 'IBAN'
    ],
];
